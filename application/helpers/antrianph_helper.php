<?php

function is_logged_in()
{
    $ph = get_instance();
    if (!$ph->session->userdata('email')) {
        redirect('administrator/auth');
    } else {
        $role_id = $ph->session->userdata('role_id');
        $menu = $ph->uri->segment(2);

        $queryMenu = $ph->db->get_where('user_menu', ['menu' => $menu])->row_array();
        $menu_id = $queryMenu['id'];

        $userAccess = $ph->db->get_where('user_access_menu', [
            'role_id' => $role_id,
            'menu_id' => $menu_id
        ]);

        if ($userAccess->num_rows() < 1) {
            redirect('administrator/auth/blocked');
        }
    }
}

function check_access($role_id, $menu_id)
{
    $ph = get_instance();

    $ph->db->where('role_id', $role_id);
    $ph->db->where('menu_id', $menu_id);
    $result = $ph->db->get('user_access_menu');

    if ($result->num_rows() > 0) {
        return "checked='checked'";
    }
}
