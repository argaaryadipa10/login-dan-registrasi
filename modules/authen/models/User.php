<?php
defined('BASEPATH') or exit('No direct script access allowed');


class User extends CI_Model
{



    $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
            'is_unique' => 'Email ini sudah terdaftar!'
        ]);
        $this->form_validation->set_rules('password1', 'Password1', 'required|trim|min_length[6]|matches[password2]', [
            'matches' => 'Password tidak sama!',
            'min_length' => 'Password telalu pendek!'
        ]);
        $this->form_validation->set_rules('password2', 'Password2', 'required|trim|matches[password1]');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Registrasi User';
            $this->load->view('authen/templates/auth_header', $data);
            $this->load->view('authen/auth/registration');
            $this->load->view('authen/templates/auth_footer');
        } else {
            $data = [
                'nrp' => $this->input->post('nrp'),
                'nama' => htmlspecialchars($this->input->post('nama', true)),
                'email' => htmlspecialchars($this->input->post('email', true)),
                'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'jabatan_id',
                'no_telp' => $this->input->post('no_telp'),
                'role_nrp' => 2,
                'is_active' => 1,
                'created_date' => time(),
                'modified_date' => time()
            ];

            // var_dump($data);
            // die;
            $this->db->insert('user', $data);

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Akun anda sudah terdaftar. Silahkan login!
            </div>');

            redirect('authen/auth');
        }
}