<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Auth extends My_Controller
{

    // $autoload = array(
    //     // 'helper' => array('url', 'file', 'security'),
    //     'libraries' => array('database', 'email', 'session')
    // );

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Login';
            $this->load->view('authen/templates/auth_header', $data);
            $this->load->view('authen/auth/login');
            $this->load->view('authen/templates/auth_footer');
        } else {
            // validasi sukses
            $this->_login();
        }

        // var_dump(base_url());
    }

    private function _login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $user = $this->db->get_where('user', ['email' => $email])->row_array();

        // user ada
        if ($user) {
            // user aktif
            if ($user['is_active'] == 1) {
                // cek password
                if (password_verify($password, $user['password'])) {
                    $data = [
                        'email' => $user['email'],
                        'role_nrp' => $user['role_nrp']
                    ];
                    $this->session->set_userdata($data);

                    // cek admin atau member
                    if ($user['role_nrp'] == 1) {
                        redirect('admin/admin');
                    } else {
                        redirect('admin/user');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    Password salah!
                    </div>');
                    redirect('admin/auth');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Email ini belum diaktifkan!
                </div>');
                redirect('admin/auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Email tidak terdaftar!
            </div>');
            redirect('admin/auth');
        }
    }

    public function registration()
    {
        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
            'is_unique' => 'Email ini sudah terdaftar!'
        ]);
        $this->form_validation->set_rules('password1', 'Password1', 'required|trim|min_length[6]|matches[password2]', [
            'matches' => 'Password tidak sama!',
            'min_length' => 'Password telalu pendek!'
        ]);
        $this->form_validation->set_rules('password2', 'Password2', 'required|trim|matches[password1]');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Registrasi User';
            $this->load->view('authen/templates/auth_header', $data);
            $this->load->view('authen/auth/registration');
            $this->load->view('authen/templates/auth_footer');
        } else {
            $data = [
                'nrp' => $this->input->post('nrp'),
                'nama' => htmlspecialchars($this->input->post('nama', true)),
                'email' => htmlspecialchars($this->input->post('email', true)),
                'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
                'jabatan_id',
                'no_telp' => $this->input->post('no_telp'),
                'role_nrp' => 2,
                'is_active' => 1,
                'created_date' => time(),
                'modified_date' => time()
            ];

            // var_dump($data);
            // die;
            $this->db->insert('user', $data);

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Akun anda sudah terdaftar. Silahkan login!
            </div>');

            redirect('authen/auth');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role_nrp');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
        Anda telah logout!
        </div>');
        redirect('authen/auth');
    }
}
