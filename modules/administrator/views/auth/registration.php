  <!-- Register Content -->
  <div class="container-login">
      <div class="row justify-content-center">
          <div class="col-xl-6 col-lg-12 col-md-9">
              <div class="card shadow-sm my-5">
                  <div class="card-body p-0">
                      <div class="row">
                          <div class="col-lg-12">
                              <div class="login-form">
                                  <div class="text-center">
                                      <h1 class="h4 text-gray-900 mb-4">Registrasi Akun</h1>
                                  </div>
                                  <form class="user" method="post" action="<?= base_url('administrator/auth/registration'); ?>">
                                      <div class="form-group">
                                          <label>NRP</label>
                                          <input type="number" class="form-control form-control-user" id="nrp" name="nrp" placeholder="Masukkan NRP" value="<?= set_value('nrp');  ?>">

                                          <?= form_error('nrp', '<small class="text-danger">', '</small>'); ?>
                                      </div>

                                      <div class="form-group">
                                          <label>Nama</label>
                                          <input type="text" class="form-control form-control-user" id="nama" name="nama" placeholder="Masukkan Nama" value="<?= set_value('nama');  ?>">

                                          <?= form_error('nama', '<small class="text-danger">', '</small>'); ?>
                                      </div>

                                      <div class="form-group">
                                          <label>Email</label>
                                          <input type="text" class="form-control form-control-user" id="email" name="email" placeholder="Masukkan Email" value="<?= set_value('email');  ?>">

                                          <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
                                      </div>

                                      <div class="form-group">
                                          <label>No. Telepon</label>
                                          <input type="number" class="form-control form-control-user" id="no_telp" name="no_telp" placeholder="Masukkan No Telepon" value="<?= set_value('no_telp');  ?>">

                                          <?= form_error('no_telp', '<small class="text-danger">', '</small>'); ?>
                                      </div>

                                      <div class="form-group row">
                                          <div class="col-sm-6 mb-3 mb-sm-0">
                                              <label>Password</label>
                                              <input type="password" class="form-control form-control-user" id="password1" name="password1" placeholder="Masukkan Password">

                                              <?= form_error('password1', '<small class="text-danger">', '</small>'); ?>
                                          </div>

                                          <div class="col-sm-6">
                                              <label>Ulangi Password</label>
                                              <input type="password" class="form-control form-control-user" id="password2" name="password2" placeholder="Masukkan Ulang Password">
                                          </div>
                                      </div>


                                      <div class="form-group">
                                          <button type="submit" class="btn btn-primary btn-block" href="<?= base_url('administrator/auth'); ?>">Registrasi</button>
                                      </div>

                                      <hr>
                                  </form>

                                  <div class="text-center">
                                      <a class="font-weight-bold medium" href="<?= base_url('administrator/auth/lupapassword'); ?>">Lupa Password?</a>
                                  </div>

                                  <div class="text-center">
                                      <a class="font-weight-bold medium" href="<?= base_url('administrator/auth'); ?>">Sudah punya akun? Login!</a>
                                  </div>
                                  <div class="text-center">
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>