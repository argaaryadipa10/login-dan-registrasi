    <!-- Login Content -->
    <div class="container-login">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-12 col-md-9">
                <div class="card shadow-sm my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="login-form">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Lupa Password?</h1>
                                    </div>

                                    <?= $this->session->flashdata('message');  ?>

                                    <form class="user" method="post" action="<?= base_url('administrator/auth/lupapassword');  ?>">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="email" name="email" placeholder="Masukkan Email" value="<?= set_value('email');  ?>">

                                            <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary btn-block">Reset Password</button>
                                        </div>
                                    </form>
                                    <hr>

                                    <div class="text-center">
                                        <a class="font-weight-bold medium" href="<?= base_url('administrator/auth');  ?>">Kembali ke login</a>
                                    </div>
                                    <div class="text-center">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>