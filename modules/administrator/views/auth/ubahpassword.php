    <!-- Login Content -->
    <div class="container-login">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-12 col-md-9">
                <div class="card shadow-sm my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="login-form">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900">Ubah password untuk </h1>
                                        <h5 class="mb-4"><?= $this->session->userdata('reset_email'); ?></h5>
                                    </div>

                                    <?= $this->session->flashdata('message');  ?>

                                    <form class="user" method="post" action="<?= base_url('administrator/auth/ubahpassword');  ?>">
                                        <!-- Password baru -->
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="password1" name="password1" placeholder="Masukkan Password Baru">

                                            <?= form_error('password1', '<small class="text-danger">', '</small>'); ?>
                                        </div>

                                        <!-- Ulangi password -->
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="password2" name="password2" placeholder="Ulangi Password">

                                            <?= form_error('password2', '<small class="text-danger">', '</small>'); ?>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary btn-block">Ubah Password</button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>