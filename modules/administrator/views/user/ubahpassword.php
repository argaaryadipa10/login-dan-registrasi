                <!-- Container Fluid-->
                <div class="container-fluid" id="container-wrapper">
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">

                        <!-- Title Dasboard -->
                        <h1 class="h3 mb-0 text-gray-800"><?= $title; ?></h1>

                    </div>

                    <!-- Form ubah password -->
                    <div class="row">
                        <div class="col-lg-6">

                            <?= $this->session->flashdata('message'); ?>

                            <form action="<?= base_url('administrator/user/ubahpassword'); ?>" method="post">
                                <!-- Password saat ini -->
                                <div class="form-group">
                                    <label for="password_saatini">Password Saat Ini</label>
                                    <input type="password" class="form-control" id="password_saatini" name="password_saatini">
                                    <?= form_error('password_saatini', '<small class="text-danger">', '</small>'); ?>
                                </div>

                                <!-- Password baru -->
                                <div class="form-group">
                                    <label for="password_baru1">Password Baru</label>
                                    <input type="password" class="form-control" id="password_baru1" name="password_baru1">
                                    <?= form_error('password_baru1', '<small class="text-danger">', '</small>'); ?>
                                </div>

                                <!-- Ulangi password -->
                                <div class="form-group">
                                    <label for="password_baru2">Ulangi Password</label>
                                    <input type="password" class="form-control" id="password_baru2" name="password_baru2">
                                    <?= form_error('password_baru2', '<small class="text-danger">', '</small>'); ?>
                                </div>

                                <!-- Button-->
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Ubah Password</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- Modal Logout -->
                    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabelLogout">Ohh No!</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>Apa anda yakin ingin logout?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>
                                    <a href="<?= base_url('administrator/auth/logout'); ?>" class="btn btn-primary">Logout</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!---Container Fluid-->
                </div>