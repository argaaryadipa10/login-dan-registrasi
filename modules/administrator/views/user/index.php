                <!-- Container Fluid-->
                <div class="container-fluid" id="container-wrapper">
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">

                        <!-- Title Dasboard -->
                        <h1 class="h3 mb-0 text-gray-800"><?= $title; ?></h1>

                    </div>

                    <!-- Flashdata -->
                    <div class="row">
                        <div class="col-lg-7">
                            <?= $this->session->flashdata('message'); ?>
                        </div>
                    </div>

                    <!-- Card -->
                    <div class="card mb-3" style="max-width: 640px;">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <img src="<?= base_url('assets/');  ?>img/boy.png" class="card-img">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-text"><?= $user['nrp']; ?></h5>
                                    <p class="card-title"><?= $user['nama']; ?></p>
                                    <p class="card-text"><?= $user['email']; ?></p>
                                    <p class="card-text"><?= $user['no_telp']; ?></p>
                                    <p class="card-text"><small class="text-muted">Member dari <?= date('d F Y', $user['created_date']);  ?></small></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal Logout -->
                    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabelLogout">Ohh No!</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>Apa anda yakin ingin logout?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>
                                    <a href="<?= base_url('administrator/auth/logout'); ?>" class="btn btn-primary">Logout</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!---Container Fluid-->
                </div>