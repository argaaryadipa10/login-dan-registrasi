<?php

class Dashboard extends MY_Controller
{
    function __construct()
    {
        parent:: __construct();
    }

    function index()
    {
        // $data['content_view'] = 'antrian/Home_v';
        $this->load->view('antrian/templates/Header_v');
        $this->load->view('antrian/templates/Sidebar_v');
        $this->load->view('antrian/templates/Topbar_v');
        $this->load->view('antrian/Dashboard_v');
        // $this->load->view('antrian/blank.php');
        $this->load->view('antrian/templates/Footer_v');
        
    }

    function coba()
    {
        $this->load->module('antrian/Coba');
        $this->coba->tambah();
        
        
    }

}